# AppImage demo project

## Usage example

```
# Configure
mkdir hello-cmake-build
cd !$
cmake path/to/hello-cmake

# Build
cmake --build .

# Package
# TODO: Figure out why the package target doesn't work
#cmake --build . --target package
cpack -G AppImage

# Test
./hello-cmake-x86_64.AppImage
```
